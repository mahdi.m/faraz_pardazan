# The work done

### Add these lines to .env

    TEST_DB_HOST=localhost
    TEST_DB_DATABASE=testing_db
    TEST_DB_USERNAME=root
    TEST_DB_PASSWORD=123456

### create "testing_db" database

### Run this command to test Authentication

    php artisan test ./Modules/Auth/Tests/Feature

### Run this command to test app

    php artisan test ./Modules/Customer/Tests/Feature

## پروژه خرید و فروش 
پروژه به این صورت است که کاربر ها از طریق یک سرویس سهمیه ریالی و محصول را دریافت میکنند
سپس میتوانند بر اساس موجودی ریالی و محصول خود اقدام به خرید و فروش نمایند.

شمای دیتابیس در قالب فایل drawio و عکس در پوشه پروژه قرار گرفته است
کالکشن postman در قالب فایل docs.postman داخل پروژه قرار گرفته است
