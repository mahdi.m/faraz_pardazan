<?php

namespace Tests;

use App\Models\Customer;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Passport\Passport;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication
        , RefreshDatabase;

    public function loginPassport($data = [])
    {
        $customer = count($data) ? Customer::factory()->create($data) : Customer::factory()->create();
        return Passport::actingAs(
            $customer,
            ['create-servers'],
            'customer'
        );
    }
}
