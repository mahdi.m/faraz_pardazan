<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExchangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchanges', function (Blueprint $table) {
            $table->id();


            $table->foreignId('customer_id')
                ->nullable()
                ->references('id')
                ->on('customers')
                ->onDelete('cascade');

            $table->foreignId('product_id')
                ->nullable()
                ->references('id')
                ->on('products')
                ->onDelete('cascade');

            $table->integer('amount')->nullable();
            $table->unsignedInteger('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_products');
    }
}
