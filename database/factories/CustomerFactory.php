<?php

namespace Database\Factories;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'username' => $this->faker->userName,
            "email" => $this->faker->email,
            'mobile' => $this->faker->regexify('9[0-9]{9}'),
            "status" => $this->faker->numberBetween(0, 1),
            "is_ban" => $this->faker->numberBetween(0, 1),
        ];
    }
}
