<?php

namespace Database\Factories;

use App\Models\LineSale;
use Illuminate\Database\Eloquent\Factories\Factory;

class LineSaleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LineSale::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
