<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\Status;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $status = Status::query();
        if (!$status->first()) {
            $status->insert([
                ['title' => 'در حال بررسی'],
                ['title' => 'انجام شده '],
                ['title' => 'فعال '],
            ]);
        }
        $defaultAmount = 10;
        $product = Product::query();
        if (!$product->first()) {
            Product::query()->create([
                'title' => 'گندم',
                'price' => $defaultAmount,
                'existence' => $defaultAmount,
//                'status_id' => $status->latest()->first()->id
            ]);
        }

    }
}
