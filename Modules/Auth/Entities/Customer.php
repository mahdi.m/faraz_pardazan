<?php

namespace Modules\Auth\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Auth\Database\factories\CustomerFactory;

class Customer extends \App\Models\Customer
{
    use HasFactory;


    protected static function newFactory()
    {
        return CustomerFactory::new();
    }
}
