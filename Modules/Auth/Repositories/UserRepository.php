<?php

namespace Modules\Auth\Repositories;

use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\Customer;

class UserRepository extends BaseRepository
{

    public function model()
    {
//        return Sample::class;
    }

    public function findModel($model, $id)
    {
        return $model::find($id);
    }

    public function findModelByMobile($model, $mobile)
    {
        return $model::query()->where('mobile', $mobile)->first();
    }

    public function findModelByUsername(string $username, string $model = Customer::class)
    {
        return $model::query()->where('username', $username)->first();
    }

    /**
     * @param Model|int $user
     * @param null $model
     * @return bool
     */
    public function isBanned($user, $model = null): bool
    {
        if (is_int($user)) {
            $user = $model::find($user) ?: new $model;
            return $user->is_ban;
        }
        return $user->is_ban;
    }

    public function existsUser(string $model, string $field)
    {
        return $model::query()->where('email', $field)->first();
    }

    public function createUser(array $data, string $model = Customer::class)
    {
        return $model::query()->create($data);
    }




}
