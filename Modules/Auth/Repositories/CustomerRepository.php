<?php

namespace Modules\Auth\Repositories;

use App\Repositories\BaseRepository;
use Modules\Auth\Entities\Customer;

class CustomerRepository extends BaseRepository
{

    public function model()
    {
        return Customer::class;
    }

    public function getByUsername(string $username)
    {
        return $this->model->where('username', $username)->firstOrFail();
    }


}
