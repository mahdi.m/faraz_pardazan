<?php

namespace Modules\Auth\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Entities\InvitationPrefix;

class AuthDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");

        // seed users
        foreach (config('auth.users') as $index => $user) {
            if (isset($user['seed'])) {
                foreach ($user['seed'] as $item) {
                    $item['password'] = Hash::make($item['password']);
                    $user['model']::query()->create($item);
                }
            }
        }
        $userPrefixes = config('auth.invitation_prefixes');
        if (!InvitationPrefix::query()->get()->count()) {
            foreach ($userPrefixes as $index => $userPrefix) {
                InvitationPrefix::query()->create(['role_name' => $index, 'prefix' => $userPrefix]);
            }
        }
    }
}
