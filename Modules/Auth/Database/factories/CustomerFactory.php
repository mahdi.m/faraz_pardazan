<?php

namespace Modules\Auth\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Auth\Entities\Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'username' => $this->faker->userName,
            'mobile' => $this->faker->regexify('9[0-9]{9}'),
            'status' => $this->faker->numberBetween(0, 1),
            'email' => $this->faker->email,
            'is_ban' => $this->faker->boolean,
            'password' => $this->faker->password

        ];
    }
}

