<?php

use App\Models\Customer;
use App\Models\User;

return [
    'name' => 'Auth',
    'users' => [ // همه جداول مربوط به کاربران
        'customer' => [
            'model' => Customer::class,
        ],
        'user' => [
            'model' => User::class,
            'seed' => [ // seed used in Modules\Auth\Database\Seeders\AuthDatabaseSeeder.php
                [
                    'name' => 'admin',
                    'email' => 'admin@admin.com',
                    'password' => '123456'
                ]
            ]
        ]
    ],
    'invitation_prefixes' => [
//        'employee' => 'emp',
        'customer' => 'cst',
    ]
];
