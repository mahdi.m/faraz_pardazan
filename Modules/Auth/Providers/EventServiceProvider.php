<?php


namespace Modules\Auth\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Events\CreateVerificationCode;
use App\Events\VerifyEmailEvent;
use App\Events\VerifyMobileEvent;
use App\Listeners\SendVerificationCode;
use App\Listeners\VerifyEmailListener;
use App\Listeners\VerifyMobileListener;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
//        CreateVerificationCode::class => [
//            SendVerificationCode::class,
//        ],
//
//        VerifyEmailEvent::class => [
//            VerifyEmailListener::class,
//        ],
//
//        VerifyMobileEvent::class => [
//            VerifyMobileListener::class,
//        ],
    ];
}
