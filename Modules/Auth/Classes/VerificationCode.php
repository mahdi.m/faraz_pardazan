<?php


namespace Modules\Auth\Classes;


use App\Models\Customer;

class VerificationCode extends \App\Classes\Abstracts\VerificationCode
{
    public static function createCode($key, string $model = Customer::class)
    {
        return parent::createCode($key, $model);
    }

}
