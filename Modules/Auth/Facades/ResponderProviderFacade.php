<?php


namespace Modules\Auth\Facades;


/**
 * Class UserProviderFacade
 * this class UserProviderFacade::shouldProxyTo(Modules\Auth\Services\Provider\UserProvider\UserProvider::class); in Modules\Auth\Providers\AuthServiceProvider
 * @package Modules\Auth\Facades
 * @method success(string $message)
 * @method created(string $message)
 * @method updated(string $message)
 * @method badRequest(string $message)
 * @method deleted(string $message)
 * @method notFound(string $message)
 * @method internalError(string $message)
 * @method unauthorizedError(string $message)
 * @method error(int $statusCode, string $message)
 */
class ResponderProviderFacade extends BaseFacade
{
    const key = 'auth.responderProvider';
}
