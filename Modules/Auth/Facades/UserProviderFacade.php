<?php


namespace Modules\Auth\Facades;


use App\Models\Customer;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserProviderFacade
 * this class UserProviderFacade::shouldProxyTo(Modules\Auth\Services\Provider\UserProvider\UserProvider::class); in Modules\Auth\Providers\AuthServiceProvider
 * @package Modules\Auth\Facades
 * @method getUserByMobile(string $model, int $mobile)
 * @method findByUsername(string $username, string $model = Customer::class)
 * @method createUserIfNotExist(Model $user, string $model, int $mobile, string $userType)
 * @method isBanned(Model $user, string $model = null)
 * @method createUserToken(Model $user)
 * @method createUser(array $data, string $model = Customer::class)
 * @method existsUser(string $username, string $model = Customer::class)
 * @method applyInvitationCode(Model $user, string $invitationCode)
 */
class UserProviderFacade extends BaseFacade
{
    const key = 'auth.userProvider';
}
