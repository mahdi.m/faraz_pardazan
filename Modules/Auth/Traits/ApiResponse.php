<?php


namespace Modules\Auth\Traits;

use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;


trait ApiResponse
{
    protected $statusCode;

    public function respondCreated(string $message, $data = null)
    {
        $this->statusCode = Response::HTTP_CREATED;
        return $this->respond([
            'message' => $message,
            'data' => $data,
        ]);

    }

    public function respondUpdated(string $message, $data = null)
    {
        $this->statusCode = Response::HTTP_OK;
        return $this->respond([
            'message' => $message,
            'data' => $data,
        ]);
    }


    public function respondSuccess(string $message, $data = null)
    {
        $this->statusCode = Response::HTTP_OK;
        return $this->respond([
            'message' => $message,
        ]);
    }

    public function respond($data)
    {
        return \response()->json(
            $data, $this->getStatusCode()
        );
    }

    public function respondDeleted(string $message, $data = null)
    {
        $this->statusCode = Response::HTTP_OK;
        return $this->respond([
            'message' => $message,
            'data' => $data,
        ]);
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function respondNotFound(string $message = 'Not Found!')
    {
        $this->statusCode = Response::HTTP_NOT_FOUND;
        return $this->respond([
            'message' => $message,
        ]);
    }

    public function respondInternalError(string $message)
    {
        $this->statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        return $this->respond([
            'message' => $message,
        ]);

    }

    public function respondValidationError(string $message, $errors)
    {
        $this->statusCode = Response::HTTP_UNPROCESSABLE_ENTITY;
        return $this->respond([
            'body' => $errors,
            'message' => $message,
        ]);
    }

    public function respondUnauthorizedError(string $message)
    {
        $this->statusCode = Response::HTTP_UNAUTHORIZED;
        return $this->respond([
            'message' => $message,

        ]);
    }

    public function respondSuccessWithBody($data, $message)
    {
        $this->statusCode = Response::HTTP_OK;
        return $this->respond([
            'data' => $data,
            'message' => $message,
        ]);
    }

    public function respondErrorWithBody($data, $message)
    {
        $this->statusCode = Response::HTTP_UNAUTHORIZED;
        return $this->respond([
            'data' => $data,
            'message' => $message,
        ]);
    }

    public function respondWithPagination(Paginator $paginate, $data, string $message = '')
    {
        $this->statusCode = Response::HTTP_OK;
        return $this->respond([
            'body' => $data,
            'message' => $message,
            'paginator' => [
                'total_count' => $paginate->total(),
                'total_pages' => ceil($paginate->total() / $paginate->perPage()),
                'current_page' => $paginate->currentPage(),
                'limit' => $paginate->perPage(),
            ],
            'status' => 'success',

        ]);
    }

}
