<?php

namespace Modules\Auth\Tests\Feature;

use App\Models\Customer;
use Illuminate\Support\Str;
use Modules\Auth\Facades\UserProviderFacade;
use Tests\TestCase;

class AuthTest extends TestCase
{
    public function testRegister()
    {
        $username = 'ssmns';
        $password = '12345678';
        $model = Customer::class;
        Customer::unguard();
//        $this->withoutExceptionHandling();

        /**********
         * ------------
         * this methods is mocking
         * ------------
         *********/
        $data = [
            'username' => $username,
            'password' => $password,
        ];

        $user = new $model([
            'id' => 1,
            'username' => $username,
            'password' => $password,
            'name' => 'test',
            'status' => true,
            'is_ban' => false,
        ]);

        Customer::factory()->create(['username' => 'test_user']);

        UserProviderFacade::shouldReceive('existsUser')
            ->once()
            ->with($username)
            ->andReturn(null);

        UserProviderFacade::shouldReceive('createUser')
            ->once()
            ->with($data)
            ->andReturn($user);


        $user->setAttribute('api_token', Str::random(20));

        UserProviderFacade::shouldReceive('createUserToken')
            ->once()
            ->with($user)
            ->andReturn($user);

        /*
        ------------
         */
        $response = $this->json(
            'POST',
            route('customer.register'),
            [
                'username' => $username,
                'password' => $password,
                'password_confirmation' => $password,
            ]
        );
        $response->assertOk();
        $this->assertInstanceOf(Customer::class, $response->getOriginalContent());
    }

    /**
     * user login api
     *
     * @return void
     */
    public function testLogin()
    {
        $this->withoutExceptionHandling();
        $username = 'ssmns';
        $password = '12345678';
        Customer::unguard();
        $model = Customer::class;

        /**********
         * ------------
         * this methods is mocking
         * ------------
         *********/
        $data = [
            'id' => 1,
            'username' => $username,
            'password' => $password,
            'name' => 'test',
            'status' => true,
        ];
        $user = Customer::factory()->create($data);

        UserProviderFacade::shouldReceive('isBanned')
            ->once()
            ->with($user)
            ->andReturn(false);

        UserProviderFacade::shouldReceive('existsUser')
            ->once()
            ->with($username)
            ->andReturn($user);

        $user->setAttribute('api_token', Str::random(10));
        UserProviderFacade::shouldReceive('createUserToken')
            ->once()
            ->with($user)
            ->andReturn($user);
        /*
        ------------
         */
        $res = $this->json('post', 'api/auth/login', ['username' => $username, 'password' => $password]);
        $res->assertStatus(201); // because create token
        $this->assertEquals($user->toJson(), $res->content());
    }


}
