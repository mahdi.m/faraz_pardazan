<?php

namespace Modules\Auth\Tests\Feature;

use Illuminate\Support\Facades\Artisan;
use Modules\Auth\Entities\Customer;
use Modules\Auth\Facades\UserProviderFacade;
use Tests\TestCase;

class UserProviderTest extends TestCase
{
    public function testExistsUser()
    {
        $user = Customer::factory()->create(['email' => 'user@user.com']);
        $existsUser = UserProviderFacade::existsUser($user->email);
        $this->assertEquals($user->toArray(), $existsUser->toArray());
    }

    public function testCreateUser()
    {
        Customer::unguard();
        $data = [
            'id' => 1,
            'email' => 'user@user.com',
            'name' => 'test',
            'status' => true,
            'is_ban' => false,
        ];
        $existsUser = UserProviderFacade::createUser($data);
        $existsUser->makeHidden(['updated_at', 'created_at']);

        $this->assertEquals($data, $existsUser->toArray());
    }


    public function testCreateUserToken()
    {
        $data = [
            'email' => 'user@user.com',
            'name' => 'test',
            'status' => true,
            'is_ban' => false,
        ];
        Artisan::call('passport:install');
        $user = Customer::factory()->create($data);
        $user = UserProviderFacade::createUserToken($user);
        $this->assertTrue(isset($user->api_token));
    }

}
