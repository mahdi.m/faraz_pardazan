## Auth Module Configuration


###to config Auth module do this steps:


#### first add your users models to config. (seed key is optional):
   **put all users in this key**
   
        'users' => [ 
                'customer' => [
                    'model' => Customer::class,
                ],
                'user' => [
                    'model' => User::class,
                    <ul><li>seed used in Modules\Auth\Database\Seeders\AuthDatabaseSeeder.php</li></ul>
                    'seed' => [ 
                        [
                            'name' => 'admin',
                            'email' => 'admin@admin.com',
                            'password' => '123456'
                        ]
                    ]
                ]
            ],
    
    
    
#### add redis to config/database.php
    
    'client' => env('REDIS_CLIENT', 'predis'),
    
    
#### create custom guard for users in config/auth.php
    'guards' => [
       'web' => [
           'driver' => 'session',
           'provider' => 'users',
       ],

       'api' => [
           'driver' => 'passport',
           'provider' => 'users',
           'hash' => false,
       ],
       // custom guard for customer
       'customer' => [
           'driver' => 'passport',
           'provider' => 'customers',
           'hash' => false,

       ],  
    ],
       
    'providers' => [
            'users' => [
                'driver' => 'eloquent',
                'model' => App\Models\User::class,
            ],
            'customers' => [
                'driver' => 'eloquent',
                'model' => App\Models\Customer::class,
            ],
    ]
   
#### add guard property tu customer model
    // my custom property
    public $guard = 'customer';

#### add users role ad descriptions in app/Providers/AuthServiceProvider.php
                                       
    public function boot()
    {
        Passport::tokensCan([
            Passport::routes();
            Passport::tokensCan([
                'customer' => 'login token for customer',
            ]);
        ]);
    }

#### use HasApiTokens in Customer.php
    use HasFactory, HasApiTokens;     
     
#### install passport
    php artisan passport:install
    
    
