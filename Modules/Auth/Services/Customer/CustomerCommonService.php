<?php


namespace Modules\Auth\Services\Customer;


use App\Services\BaseService;
use App\Repositories\BaseRepository;
use Modules\Auth\Repositories\CustomerRepository;

class CustomerCommonService extends BaseService
{
    public BaseRepository $repository;

    public function __construct(CustomerRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $email
     * @return mixed
     */
    protected function validateEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @param $mobile
     * @return false|int
     */
    protected function validateMobile($mobile)
    {
        return preg_match('/^[0-9]{11}+$/', $mobile);
    }


}
