<?php


namespace Modules\Auth\Services\Customer;


use App\Services\BaseService;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Events\VerifyEmailEvent;
use App\Events\VerifyMobileEvent;
use Modules\Auth\Facades\ResponderProviderFacade;
use Modules\Auth\Facades\VerificationCodeProviderFacade;
use Modules\Auth\Repositories\CustomerRepository;

class CustomerResetPasswordService extends CustomerCommonService
{
    public function resetPassword(Request $request)
    {
        $sendMethod = $request->send_method;
        $verificationCode = VerificationCodeProviderFacade::hasActiveCode($sendMethod);
        if (!$verificationCode) {
            return ResponderProviderFacade::notFound('کد تایید یافت نشد لطفا دوباره تلاش کنید');
        }

        if ($this->validateEmail($sendMethod)) {
            $user = $this->repository->firstBy('email', $sendMethod);
            $user->update(['password' => $request->password]);
            return $user;
        }

        if ($this->validateMobile($sendMethod)) {
            $user = $this->repository->firstBy('mobile', $sendMethod);
            $user->update(['password' => $request->password]);
            return $user;
        }

        return ResponderProviderFacade::error(422, __('messages.response.invalid-data'));
    }
}
