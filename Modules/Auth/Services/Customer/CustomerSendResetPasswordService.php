<?php


namespace Modules\Auth\Services\Customer;


use App\Services\BaseService;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Events\VerifyEmailEvent;
use App\Events\VerifyMobileEvent;
use Modules\Auth\Facades\ResponderProviderFacade;
use Modules\Auth\Facades\VerificationCodeProviderFacade;
use Modules\Auth\Repositories\CustomerRepository;

class CustomerSendResetPasswordService extends CustomerCommonService
{
    public function sendResetPasswordVerification(\Modules\Auth\Http\Requests\SendResetPasswordVerificationRequest $request)
    {
        $sendMethod = $request->send_method;
        $hasActiveCode = VerificationCodeProviderFacade::hasActiveCode($sendMethod);
        if ($hasActiveCode) {
            return ResponderProviderFacade::error(Response::HTTP_NOT_ACCEPTABLE,'کد تایید قبلا برای شما ارسال شده است');
        }

        if ($this->validateEmail($sendMethod)) {
            event(new VerifyEmailEvent($sendMethod));
            return ResponderProviderFacade::success(__('messages.verify-response.send'));
        }

        if ($this->validateMobile($sendMethod)) {
            event(new VerifyMobileEvent($sendMethod));
            return ResponderProviderFacade::success(__('messages.verify-response.send'));
        }

        return ResponderProviderFacade::error(422,__('messages.response.invalid-data'));

    }

    /**
     * @param $email
     * @return mixed
     */
    protected function validateEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @param $mobile
     * @return false|int
     */
    protected function validateMobile($mobile)
    {
        return preg_match('/^9[0-9]{9}$/', $mobile);
    }

}
