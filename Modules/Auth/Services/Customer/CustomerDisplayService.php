<?php


namespace Modules\Auth\Services\Customer;


use App\Services\BaseService;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Modules\Auth\Repositories\CustomerRepository;

class CustomerDisplayService extends CustomerCommonService
{
    public function getByUsername(Request $request)
    {
        $username = $request->username;
        return $this->repository->getByUsername($username);
    }
}
