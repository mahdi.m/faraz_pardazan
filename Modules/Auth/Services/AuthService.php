<?php


namespace Modules\Auth\Services;

use App\Services\BaseService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Facades\ResponderProviderFacade;
use Modules\Auth\Facades\UserProviderFacade;
use Modules\Auth\Http\Requests\RegisterRequest;
use Symfony\Component\HttpFoundation\Response as Res;

class AuthService extends BaseService
{

    public function loginUser(Request $request)
    {
        $existsUser = UserProviderFacade::existsUser($request->username);

        if (UserProviderFacade::isBanned($existsUser)) {
            return ResponderProviderFacade::badRequest('این کاربر بلاک شده است');
        }

        if (!$existsUser) {
            return ResponderProviderFacade::error(404, 'کاربری یافت نشد. لطفاثبت نام کنید');
        }

        if (Hash::check($request->password, $existsUser->password)) {
            return UserProviderFacade::createUserToken($existsUser);
        }
        return ResponderProviderFacade::unauthorizedError('اطلاعات غلط است');
    }

    public function registerUser(RegisterRequest $request)
    {
        $existUser = UserProviderFacade::existsUser($request->username);
        if ($existUser) return ResponderProviderFacade::error(Res::HTTP_METHOD_NOT_ALLOWED, 'این کاربر قبلا ثبت شده');

        $createdUser = UserProviderFacade::createUser($request->except('password_confirmation'));


        return UserProviderFacade::createUserToken($createdUser);

    }


}
