<?php


namespace Modules\Auth\Services\Provider;


use Modules\Auth\Entities\Customer;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserProvider
 * @package Modules\Auth\Services\Provider
 */
class UserProvider extends CommonProvider
{

    public function getUserByMobile($model, $mobile)
    {
        return $this->repository->findModelByMobile($model, $mobile);
    }

    public function findByUsername(string $username, string $model = Customer::class)
    {
        return $this->repository->findModelByUsername($model, $username);
    }

    public function createUserIfNotExist($user, $model, $mobile, $userType)
    {
        if ($user) return $user;

        $u = $model::query()->create([
            'mobile' => $mobile,
            'status' => false,
            'is_ban' => false,
        ]);
        // ایجاد کیف پول برای کاربر
        $u->wallet()->create(['value' => 0]);
        return $u;
    }

    public function isBanned(Model $user, $model = null)
    {
        return $this->repository->isBanned($user, $model);
    }

    public function createUserToken($user)
    {
        $token = $user->createToken('user api token', [$user->guard])->accessToken;
        $user->setAttribute('api_token', $token);
        return $user;
    }

    public function existsUser(string $username, string $model = Customer::class)
    {
        return $this->repository->existsUser($model, $username);
    }

    public function createUser(array $data)
    {
        return $this->repository->createUser($data);
    }

}
