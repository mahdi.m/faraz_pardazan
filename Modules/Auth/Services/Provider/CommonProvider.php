<?php


namespace Modules\Auth\Services\Provider;


use App\Services\BaseService;
use Modules\Auth\Repositories\UserRepository;
use App\Repositories\BaseRepository;

class CommonProvider extends BaseService
{
    public BaseRepository $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }


}
