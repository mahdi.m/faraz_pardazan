<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Modules\Auth\Facades\ResponderProviderFacade;

class AuthController extends Controller
{

    /**
     * @param $email
     * @return mixed
     */
    protected function validateEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @param $mobile
     * @return false|int
     */
    protected function validateMobile($mobile)
    {
        return preg_match('/^[0-9]{11}+$/', $mobile);
    }

    protected function checkCodeTrust($activationCode)
    {
        // چون برای ذخیره کد ها از ردیس استفاده کردیم شرط ها رو تغییر دادیم
        // اگر کد وجود نداشت
        if (!$activationCode) {
            return ResponderProviderFacade::notFound('چنین کدی یافت نشد');
        }
        return true;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('auth::index');
    }

}
