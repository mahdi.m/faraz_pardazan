<?php

namespace Modules\Auth\Http\Controllers\Auth;

use Modules\Auth\Http\Requests\LoginRequest;
use Modules\Auth\Http\Requests\RegisterRequest;
use Modules\Auth\Services\AuthService;
use Modules\Auth\Traits\ApiResponse;

/**
 * Class AuthController
 * @package Modules\Auth\Http\Controllers\Auth
 * @group Auth
 */
class AuthController extends \Modules\Auth\Http\Controllers\AuthController
{
    use ApiResponse;

    /**
     * Customer Register
     *
     * @bodyParam username string required username
     * @bodyParam password string required password
     * @bodyParam password_confirmation string required password confirmation
     */
    public function register(RegisterRequest $request, AuthService $authService)
    {
        return $authService->registerUser($request);
    }

    /**
     * Customer login
     *
     * @bodyParam  username string required username
     * @bodyParam password string required password
     * @param LoginRequest $request
     * @param AuthService $authService
     * @return mixed
     */
    public function login(LoginRequest $request, AuthService $authService)
    {
        return $authService->loginUser($request);
    }




}
