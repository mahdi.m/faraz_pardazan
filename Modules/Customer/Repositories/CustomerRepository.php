<?php

namespace Modules\Customer\Repositories;

use App\Repositories\BaseRepository;
use Modules\Customer\Entities\Customer;

class CustomerRepository extends BaseRepository
{
    public function model()
    {
        return Customer::class;
    }

}
