<?php

namespace Modules\Customer\Repositories;

use App\Repositories\BaseRepository;
use Modules\Customer\Entities\Exchange;

class ExchangeRepository extends BaseRepository
{
    public function model()
    {
        return Exchange::class;
    }


}
