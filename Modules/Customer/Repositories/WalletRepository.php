<?php

namespace Modules\Customer\Repositories;

use App\Repositories\BaseRepository;
use Modules\Customer\Entities\Wallet;

class WalletRepository extends BaseRepository
{
    public function model()
    {
        return Wallet::class;
    }

}
