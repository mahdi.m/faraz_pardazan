<?php

namespace Modules\Customer\Repositories;

use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Product;
use App\Models\UsedDiscount;
use App\Repositories\BaseRepository;
use Modules\Customer\Entities\Discount;
use Modules\Customer\Entities\Order;
use Modules\Customer\Entities\OrderItem;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class OrderRepository extends BaseRepository
{
    public function model()
    {
        return Order::class;
    }

    public function paginateOrders(int $perPage, array $relations = [])
    {
        $user = auth()->user();

        if (!$relations) return QueryBuilder::for($user->orders()->latest()->withCount('items'))
            ->allowedFilters([
                AllowedFilter::exact('status'),
            ])->paginate($perPage);
        return QueryBuilder::for($user->orders()->latest()->withCount('items')->with($relations))
            ->allowedFilters([
                AllowedFilter::exact('status'),
            ])->paginate($perPage);
    }

    public function indexOrders(array $relations = [])
    {
        $user = auth()->user();
        if (!$relations) return QueryBuilder::for($user->orders()->latest()->withCount('items'))
            ->allowedFilters([
                AllowedFilter::exact('status'),
            ])->get();
        return QueryBuilder::for($user->orders()->latest()->withCount('items')->with($relations))
            ->allowedFilters([
                AllowedFilter::exact('status'),
            ])->get();
    }

    public function storeOrder($user, array $data)
    {
        $order = $user->orders()->create($data);
        return $order;
    }

    public function updateOrder(Order $order, array $data)
    {
        $order->update($data);
        return $order;
    }

    public function storeOrderItems($order, array $productsIdCountPriceWithDiscount)
    {
        $items = [];
        foreach ($productsIdCountPriceWithDiscount as $productId => $item) {
            $items[] = new OrderItem([
                'product_id' => $productId,
                'count' => $item[0],
                'price' => $item[1],
                'price_with_discount' => $item[2],
            ]);
        }
        $order->items()->saveMany($items);
        return $order->items;
    }

    public function getProductById(array $ids)
    {
        return Product::query()->whereIn('id', $ids)->with('pack')->get();
    }

    public function findDiscountByCode($customer, string $discountCode, int $orderMinimumAmount)
    {
        $usedDiscounts = $customer->usedDiscounts->pluck('discount_id')->toArray();
        return Discount::query()->whereNotIn('id', $usedDiscounts)->where('code', $discountCode)->whereColumn('count', '>', 'number_used')->where('order_minimum_amount', '<=', $orderMinimumAmount)->firstOrFail();
    }

    public function cloneOrderFromCart(Cart $cart): \App\Models\Order
    {
        $clone = $cart->replicate();
        $order = new \App\Models\Order($clone->toArray());
        $order->save();
        return $order;
    }

    public function cloneOrderItemFromCartItem(CartItem $cartItem, int $productPriceWithDiscount): \App\Models\OrderItem
    {
        $product = $cartItem->product;
        $cloneItem = $cartItem->replicate();
        $data = array_merge($cloneItem->toArray(), ['price' => $product->price, 'price_with_discount' => $productPriceWithDiscount]);
        return new \App\Models\OrderItem($data);
    }

    public function changeDiscountTheOneUsedForTheUser(?\Illuminate\Contracts\Auth\Authenticatable $customer, $discount)
    {
        $customer->usedDiscounts()->create(['discount_id' => $discount->id]);
        $discount->increment('number_used');
//        $customer->usedDiscounts()->save(new UsedDiscount(['discount_id' => $discount->id]));
        return $customer->usedDiscounts;
    }
}
