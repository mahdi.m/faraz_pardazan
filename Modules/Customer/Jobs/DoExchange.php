<?php

namespace Modules\Customer\Jobs;

use App\Facades\ResponderProviderFacade;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Response;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Modules\Customer\Entities\Exchange;

class DoExchange implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public Exchange $exchange;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Exchange $exchange)
    {
        $this->exchange = $exchange;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            DB::beginTransaction();
            $user = auth()->user();
            $user->load('wallet');
            $user->wallet->decrement('value', $this->exchange->price);
            $user->products()->updateExistingPivot($this->exchange->product_id, ['amount' => $this->exchange->amount]);
            $this->exchange->customer->wallet->increment('value', $this->exchange->price);
            $this->exchange->delete();
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            //@TODO send notification or something
        }
    }
}
