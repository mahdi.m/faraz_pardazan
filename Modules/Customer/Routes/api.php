<?php

use Illuminate\Support\Facades\Route;
use Modules\Customer\Http\Controllers\ExchangeController;
use Modules\Customer\Http\Controllers\WalletController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('customer')->group(function () {

    Route::middleware('auth:customer')->group(function () {

        Route::post('wallet-charge', [WalletController::class, 'charge'])->name('wallet.charge');

        Route::post('send-to-exchange', [ExchangeController::class, 'sendToExchange'])->name('send.to.exchange');
        Route::post('buy-from-exchange', [ExchangeController::class, 'buyFromExchange'])->name('buy.from.exchange');

    });
});
