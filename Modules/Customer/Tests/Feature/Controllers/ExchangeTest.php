<?php

namespace Modules\Customer\Tests\Feature\Controllers;

use Modules\Customer\Entities\Customer;
use Modules\Customer\Entities\Exchange;
use Modules\Customer\Entities\Product;
use Modules\Customer\Entities\Wallet;
use Modules\Customer\Facades\WalletProviderFacade;
use Tests\TestCase;

class ExchangeTest extends TestCase
{
    public function testSendToExchange()
    {
        $user = $this->loginPassport();



        $defaultAmount = 10;

        $product = Product::factory()->create([
            'title' => 'گندم',
            'price' => $defaultAmount,
            'existence' => $defaultAmount,
        ]);

        $user->products()->sync([$product->id, ['amount' => 2]]);

        $user->wallet()->create(['value' => 10]);

        $res = $this->json('POST', route('send.to.exchange'), ['amount' => 1, 'product_id' => $product->id]);
        $res->assertCreated();
        $this->assertDatabaseHas(Exchange::class, [
            'customer_id' => $user->id,
            'product_id' => $product->id
        ]);
    }

    public function testBuyFromExchange()
    {
        $this->withoutExceptionHandling();
        $user = $this->loginPassport();
        $user->wallet()->create(['value' => 50]);
        Product::unguard();
        $product = Product::factory()->create();
        $customer = Customer::factory()->has(Wallet::factory())->create();
        $exchange = Exchange::factory()->create();
        $exchange->product()->associate($product)->save();
        $exchange->customer()->associate($customer)->save();
        $customer->products()->sync([$product->id => ['amount' => 3]]);

        $exchange->load('product', 'customer');
        $res = $this->json('POST', route('buy.from.exchange', ['exchange_id' => $exchange->id]));
        $res->assertOk()
            ->assertJson(['message' => __('messages.response.success')]);
        $this->assertDatabaseMissing(Exchange::class, ['id' => $exchange->id]);
    }
}
