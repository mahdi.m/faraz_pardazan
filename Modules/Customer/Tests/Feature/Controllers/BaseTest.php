<?php

namespace Modules\Customer\Tests\Feature\Controllers;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\Passport\Passport;
use Modules\Customer\Entities\Customer;
use Tests\TestCase;

class BaseTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * apply actingAs method for passport
     * actually login user
     */
    protected function getPassportActingAs()
    {
       return Passport::actingAs(
            Customer::factory()->create(),
            ['create-servers'],
            'customer'
        );
    }
}
