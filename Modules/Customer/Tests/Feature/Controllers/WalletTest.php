<?php

namespace Modules\Customer\Tests\Feature\Controllers;

use Modules\Customer\Entities\Product;
use Modules\Customer\Facades\WalletProviderFacade;
use Tests\TestCase;

class WalletTest extends TestCase
{
    public function testCharge()
    {
        $user = $this->loginPassport();

        $defaultAmount = 10;
        Product::factory()->create([
            'title' => 'گندم',
            'price' => $defaultAmount,
            'existence' => $defaultAmount,
        ]);

        $user->wallet()->create(['value' => 10]);

        WalletProviderFacade::shouldReceive('charge')
            ->once()
            ->andReturn($user->load('wallet'));
        /*
        ------------
         */
        $res = $this->json('post', route('wallet.charge'));
        $res->assertOk();
        $res->assertJson($user->toArray());
        $this->assertEquals($user->toJson(), $res->content());
    }
}
