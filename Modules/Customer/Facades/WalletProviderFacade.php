<?php


namespace Modules\Customer\Facades;


use App\Models\Customer;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserProviderFacade
 * this class UserProviderFacade::shouldProxyTo(Modules\Auth\Services\Provider\UserProvider\UserProvider::class); in Modules\Auth\Providers\AuthServiceProvider
 * @package Modules\Auth\Facades
 * @method charge()
 */
class WalletProviderFacade extends BaseFacade
{
    const key = 'wallet.chargeProvider';
}
