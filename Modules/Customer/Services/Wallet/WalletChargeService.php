<?php


namespace Modules\Customer\Services\Wallet;



use Modules\Customer\Facades\WalletProviderFacade;

class WalletChargeService extends WalletCommonService
{
    public function charge()
    {
        return WalletProviderFacade::charge();
    }
}
