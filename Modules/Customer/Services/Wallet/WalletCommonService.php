<?php


namespace Modules\Customer\Services\Wallet;


use App\Models\Product;
use App\Repositories\BaseRepository;
use App\Services\BaseService;
use Modules\Customer\Repositories\WalletRepository;

class WalletCommonService extends BaseService
{
    public BaseRepository $repository;

    public function __construct(WalletRepository $repository)
    {
        $this->repository = $repository;
    }

}
