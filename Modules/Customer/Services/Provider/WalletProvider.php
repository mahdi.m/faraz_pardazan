<?php


namespace Modules\Customer\Services\Provider;


use Modules\Auth\Entities\Customer;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserProvider
 * @package Modules\Auth\Services\Provider
 */
class WalletProvider extends CommonProvider
{

    public function charge()
    {
        $defaultAmount = 10;
        $user = auth()->user();
        $wheat = $this->wheat();
        $user->load('wallet', 'products');
        $user->wallet->increment('value', $defaultAmount);
        $user->products()->sync([$wheat->id => ['amount' => $defaultAmount]]);
        return $user;
    }


}
