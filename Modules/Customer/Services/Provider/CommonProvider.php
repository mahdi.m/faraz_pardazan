<?php


namespace Modules\Customer\Services\Provider;


use App\Services\BaseService;
use App\Repositories\BaseRepository;
use Modules\Customer\Entities\Product;

class CommonProvider extends BaseService
{
//    public BaseRepository $repository;
//
//    public function __construct(UserRepository $repository)
//    {
//        $this->repository = $repository;
//    }

    public function wheat()
    {
        return Product::query()->first();
    }

}
