<?php


namespace Modules\Customer\Services\Exchange;


use App\Facades\ResponderProviderFacade;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Modules\Customer\Entities\Exchange;
use Modules\Customer\Entities\Product;
use Modules\Customer\Jobs\DoExchange;

class ExchangeBuyService extends ExchangeCommonService
{
    public function buy(Request $request)
    {
        $user = auth()->user();
        $user->load('wallet');
        $exchange = $this->repository->findModel(Exchange::class, $request->exchange_id);
        $exchange->load('product', 'customer.products', 'customer.wallet');

        if ($user->wallet->value < $exchange->price) {
            return ResponderProviderFacade::error(Response::HTTP_NOT_FOUND, __('messages.response.not-enough'));
        }
        dispatch(new DoExchange($exchange));

        return ResponderProviderFacade::success(__('messages.response.success'));

    }

}
