<?php


namespace Modules\Customer\Services\Exchange;


use App\Models\Product;
use App\Repositories\BaseRepository;
use App\Services\BaseService;
use Modules\Customer\Repositories\ExchangeRepository;

class ExchangeCommonService extends BaseService
{
    public BaseRepository $repository;

    public function __construct(ExchangeRepository $repository)
    {
        $this->repository = $repository;
    }

}
