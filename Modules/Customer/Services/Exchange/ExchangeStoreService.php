<?php


namespace Modules\Customer\Services\Exchange;


use App\Facades\ResponderProviderFacade;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Customer\Entities\Product;

class ExchangeStoreService extends ExchangeCommonService
{
    public function store(Request $request)
    {
        $user = auth()->user();
        $product = $user->products()->where('product_id', $request->product_id)->first();
        if (!$product) {
            return ResponderProviderFacade::error(Response::HTTP_NOT_FOUND, __('messages.response.product-not-exists'));
        }

        list($price, $amount) = $this->getPriceAmount($product, $request);
        $user->products()->updateExistingPivot($product->id, ['amount' => $amount]);

        $data = $this->mergeData($price, $user, $request);

        return $this->repository->store($data);

    }

    /**
     * @param $product
     * @param Request $request
     * @return array
     */
    private function getPriceAmount($product, Request $request): array
    {
        $price = $product->price * $request->amount;
        $amount = $product->pivot->amount - $request->amount;
        return array($price, $amount);
    }

    /**
     * @param $price
     * @param \Illuminate\Contracts\Auth\Authenticatable|null $user
     * @param Request $request
     * @return array
     */
    private function mergeData($price, ?\Illuminate\Contracts\Auth\Authenticatable $user, Request $request): array
    {
        $array = ['price' => $price, 'customer_id' => $user->id];
        $data = array_merge($request->all(), $array);
        return $data;
    }

}
