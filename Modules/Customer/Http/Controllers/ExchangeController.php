<?php

namespace Modules\Customer\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Customer\Http\Requests\ByFromExchangeRequest;
use Modules\Customer\Http\Requests\SendToExchangeRequest;
use Modules\Customer\Services\Exchange\ExchangeBuyService;
use Modules\Customer\Services\Exchange\ExchangeStoreService;

/**
 * @group Customer
 * @authenticated
 */
class ExchangeController extends Controller
{
    /**
     * Send To Exchange
     *
     *
     * @bodyParam amount integer required Amount of the product
     * @bodyParam product_id integer required The id of the product
     *
     * @param SendToExchangeRequest $request
     * @param ExchangeStoreService $exchangeStoreService
     * @return mixed
     */
    public function sendToExchange(SendToExchangeRequest $request, ExchangeStoreService $exchangeStoreService)
    {
        return $exchangeStoreService->store($request);
    }

    /**
     * Buy from exchange
     *
     * @bodyParam exchange_id integer required The id of the exchange
     * @param ByFromExchangeRequest $request
     * @param ExchangeBuyService $exchangeBuyService
     * @return mixed
     */
    public function buyFromExchange(ByFromExchangeRequest $request, ExchangeBuyService $exchangeBuyService)
    {
        return $exchangeBuyService->buy($request);
    }
}
