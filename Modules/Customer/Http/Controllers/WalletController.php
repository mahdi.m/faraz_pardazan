<?php

namespace Modules\Customer\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Customer\Services\Wallet\WalletChargeService;

/**
 * @group Customer
 */
class WalletController extends Controller
{
    /**
     * Charge wallet
     *
     * @param WalletChargeService $walletChargeService
     * @return mixed
     */
    public function charge(WalletChargeService $walletChargeService)
    {
        return $walletChargeService->charge();
    }
}
