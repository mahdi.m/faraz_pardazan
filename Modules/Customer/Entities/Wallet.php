<?php

namespace Modules\Customer\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Customer\Database\factories\WalletFactory;

class Wallet extends \App\Models\Wallet
{
    use HasFactory;

    protected static function newFactory()
    {
        return WalletFactory::new();
    }
}
