<?php

namespace Modules\Customer\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Customer\Database\factories\CustomerFactory;

class Customer extends \App\Models\Customer
{
    use HasFactory;

    protected static function newFactory()
    {
        return CustomerFactory::new();
    }
}
