<?php

namespace Modules\Customer\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Customer\Database\factories\ProductFactory;

class Product extends \App\Models\Product
{
    use HasFactory;

    protected static function newFactory()
    {
        return ProductFactory::new();
    }
}
