<?php

namespace Modules\Customer\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Exchange extends \App\Models\Exchange
{
    use HasFactory;


    protected static function newFactory()
    {
        return \Modules\Customer\Database\factories\ExchangeFactory::new();
    }
}
