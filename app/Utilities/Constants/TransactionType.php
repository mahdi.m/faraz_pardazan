<?php

namespace App\Utilities\Constants;

class TransactionType
{
    public const INVITER_GIFT = [
        'type' => 100,
        'message' => 'دریافت هدیه از دعوت کردن دوستان',
        'title' => 'inviter_gift',
    ];

    public const INVITED_GIFT = [
        'type' => 110,
        'message' => 'دریافت هدیه از دعوت شدن توسط دوستان',
        'title' => 'invited_gift',
    ];

}
