<?php

if (!function_exists('createCustomerToken')) {
    /**
     * create customer login token
     *
     * ایجاد توکن ورورد برای کاربر
     *
     * @param string $mobile
     * @return void
     */
    function createCustomerToken(string $mobile)
    {
        $user = \App\Models\Customer::whereMobile($mobile)->first();
        return $user->createToken('user api token', ['customer'])->accessToken;
    }
}

if (!function_exists('createUserToken')) {
    /**
     * create customer login token
     *
     * ایجاد توکن ورورد برای کاربر
     *
     * @param string $email
     * @return void
     */
    function createUserToken(string $email)
    {
        $user = \App\Models\User::whereEmail($email)->first();
        return $user->createToken('panel token')->accessToken;
    }
}
