<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Throwable $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
//                    dd($exception);
        // اگر مود دیباگ فعال باشد
        if (!env('APP_DEBUG', false)) {
            // چک کردن نوع درخواست که اگر جی سون باشدچه اتفاقی بیفتد
            if ($request->wantsJson()) {   //add Accept: application/json in request
                return $this->handleApiException($request, $exception);
            } else {
                $retval = parent::render($request, $exception);
            }

            return $retval;
        }

        return parent::render($request, $exception);
    }

    private function handleApiException($request, Throwable $exception)
    {

        $exception = $this->prepareException($exception);
        // اگر ارور از نوع http بود
        if ($exception instanceof HttpResponseException) {
            $exception = $exception->getResponse();
        }

        // اگر ارور از نوع Auth بود
        if ($exception instanceof AuthenticationException) {
            $exception = $this->unauthenticated($request, $exception);
        }

        // اگر ارور از نوع اعتبار سنجی بود
        if ($exception instanceof ValidationException) {
            $exception = $this->convertValidationExceptionToResponse($exception, $request);
        }
        if ($exception instanceof ModelNotFoundException) {
            $exception = $exception->getResponse();
        }
//        dd($exception);
        return $this->customApiResponse($exception);
    }

    private function customApiResponse($exception)
    {
//        dd(method_exists($exception, 'getStatusCode'));
        if (method_exists($exception, 'getStatusCode')) {
            $statusCode = $exception->getStatusCode();
        } else {
            $statusCode = 500;
        }

        $response = [];

        switch ($statusCode) {
            case 401:
                $response['message'] = 'Unauthorized';
                break;
            case 403:
                $response['message'] = 'Forbidden';
                break;
            case 404:
                $response['message'] = 'Not Found';
                break;
            case 405:
                $response['message'] = 'Method Not Allowed';
                break;
            case 422:
                $response['message'] = ' .اطلاعات صحیح نمیباشد';
//                $response['message'] = $exception->original['message'];
                $response['errors'] = $exception->original['errors'];
                break;
            default:
                $response['message'] = ($statusCode == 500) ? 'Whoops, looks like something went wrong' : $exception->getMessage();
                break;
        }

        if (config('app.debug')) {
            $response['trace'] = $exception->getTrace();
            $response['code'] = $exception->getCode();
        }


        $response['status'] = $statusCode;

        return response()->json($response, $statusCode);

    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return $request->expectsJson()
            ? response()->json([
                'message' => 'شما اجازه دسترسی ندارید (لطفا لاگین کنید)',
            ], 401)
            : redirect()->guest($exception->redirectTo() ?? route('login'));
    }


}
