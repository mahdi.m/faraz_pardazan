<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

/**
 * add user_type to fillable of the models
 */
class FixMessenger extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:messenger
                            {--rollback : Rollback the Messenger fix}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'fix cmgmyr/messenger Messenger for add user type';

    protected $messenger_path = 'vendor/cmgmyr/messenger/src/Models/';

    protected $models = [
        'Message' => [
            'searchFor' => "['thread_id', 'user_id', 'body']",
            'replace' => "['thread_id', 'user_id', 'body', 'user_type']"
        ],
        'Participant' => [
            'searchFor' => "['thread_id', 'user_id', 'last_read']",
            'replace' => "['thread_id', 'user_id', 'last_read', 'user_type']"
        ]
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!$this->option('rollback')) {
            $this->fixFiles();
            $this->info("Messenger Files have been fixed");
        } else {

            $this->rollbackFiles();
            $this->info("Messenger Files have been rolled back");
        }
    }

    private function fixFiles()
    {
        foreach ($this->models as $model => $data) {
            $searchfor = $data['searchFor'];
            $replace = $data['replace'];
            $matches = $this->replaceFillables($model, $searchfor, $replace);
            //$this->info($filename . " has been Checked/Modified");
        }
    }

    private function rollbackFiles()
    {
        foreach ($this->models as $model => $data) {
            $searchfor = $data['replace'];
            $replace = $data['searchFor'];
            $matches = $this->replaceFillables($model, $searchfor, $replace);
            //$this->info($filename . " has been Checked/Modified");
        }
    }

    /**
     * @param string $model
     * @param string $searchfor
     * @param $matches
     * @param string $replace
     * @return mixed
     */
    private function replaceFillables(string $model, string $searchfor, string $replace)
    {
        $file = base_path($this->messenger_path . $model . '.php');
        $content = file_get_contents($file);
        // escape special characters in the query
        $pattern = preg_quote($searchfor, '/');
        // finalise the regular expression, matching the whole line
        $pattern = "/^.*$pattern.*\$/m";
        if (preg_match_all($pattern, $content, $matches)) {
            file_put_contents($file, str_replace($searchfor, $replace, $content));
//            echo "Found matches:\n";
        } else {
            echo "No matches found for " . $model . "\n";
        }
        return $matches;
    }
}
