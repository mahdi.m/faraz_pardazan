<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class FixLikerUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:liker-users
    {--rollback : Rollback the Liker users fix}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "fix overtrue/laravel-like Overtrue\LaravelLike\Traits\Likeable.php for add Customer config('auth.providers.users.model')";

    protected $path = "vendor/overtrue/laravel-like/src/Traits/Likeable.php";

    protected $search = "config('auth.providers.users.model')";
    protected $replace = "config('auth.providers.customers.model')";
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!$this->option('rollback')) {
            $this->fixFiles();
            $this->info("Liker Files have been fixed");
        } else {

            $this->rollbackFiles();
            $this->info("Liker Files have been rolled back");
        }
    }

    private function fixFiles()
    {
        $matches = $this->replaceFillables($this->search, $this->replace);
    }

    private function rollbackFiles()
    {
        $matches = $this->replaceFillables($this->replace, $this->search);
    }

    private function replaceFillables(string $searchfor, string $replace)
    {
        $file = base_path($this->path);
        $content = file_get_contents($file);
        // escape special characters in the query
        $pattern = preg_quote($searchfor, '/');
        // finalise the regular expression, matching the whole line
        $pattern = "/^.*$pattern.*\$/m";
        if (preg_match_all($pattern, $content, $matches)) {
            file_put_contents($file, str_replace($searchfor, $replace, $content));
            //            echo "Found matches:\n";
        } else {
            echo "No matches found for " . $this->path . "\n";
        }
        return $matches;
    }
}
