<?php


namespace App\Services;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Modules\App\Entities\Config;

/**
 * Class UserProvider
 * @package Modules\Auth\Services\Provider
 */
class TransactionProvider
{
    /**
     * ایجاد تراکنش
     * @param Model $user
     * @param int $type
     * @param int $value
     * @param $transactionId
     * @param string $title
     * @return mixed
     */
    public function create(Model $user, int $type, int $value, string $title, $transactionId = null)
    {
        $data = [
            'type' => $type,
            'value' => $value,
            'title' => $title,
        ];
        if (isset($transactionId)) {
            $data = array_merge($data, ['transaction_id' => $transactionId,]);
        }
        return $user->transactions()->create($data);
    }
}
