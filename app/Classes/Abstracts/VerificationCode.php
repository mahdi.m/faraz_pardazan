<?php

namespace App\Classes\Abstracts;

use App\Models\Customer;
use Illuminate\Support\Facades\Cache;

abstract  class VerificationCode
{
    /**
     * create code
     * @param $key
     * @param string $model
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public static function createCode($key, string $model = Customer::class)
    {
        $codeExpireTime = 120; // 120 second
        $code = self::code();

        $data = [
            'model' => $model,
            'key' => $key,
            'code' => $code,
        ];
        $key = $model . $key;

        Cache::store('redis')->put($key, $data, $codeExpireTime);
        $val = Cache::store('redis')->get($key);
        return $val;
    }


    /**
     * @return int
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    private static function code()
    {
        do {
            $code = mt_rand(100000, 999999);
            $checkCode = Cache::store('redis')->get($code);
        } while ($checkCode);
        return $code;
    }
}
