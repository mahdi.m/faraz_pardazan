<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Mail;
use Modules\Auth\Emails\VerifyEmail;
use App\Events\VerifyEmailEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\VerifyMobileEvent;
use Modules\Auth\Http\Controllers\Verification\SmsController;

class VerifyMobileListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param VerifyEmailEvent $event
     * @return void
     */
    public function handle(VerifyMobileEvent $event)
    {
        $sms = resolve(SmsController::class);
        $sms->verification($event->verify['key'], ['code' => $event->verify['code']]);

    }
}
