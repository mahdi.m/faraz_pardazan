<?php

namespace App\Listeners;

use App\Events\CreateVerificationCode;
use Modules\Auth\Http\Controllers\Verification\SmsController;


class SendVerificationCode
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param CreateVerificationCode $event
     * @return void
     */
    public function handle(CreateVerificationCode $event)
    {
        $sms = new SmsController();
        $sms->verification($event->verificationCode['mobile'], $event->verificationCode);
    }
}
