<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Mail;
use Modules\Auth\Emails\VerifyEmail;
use App\Events\VerifyEmailEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param VerifyEmailEvent $event
     * @return void
     */
    public function handle(VerifyEmailEvent $event)
    {

        $details = [
            'title' => 'Gjoy.com',
            'body' => 'با سلام کد شما عبارت است از:',
            'code' => $event->verify['code']
        ];
//$event->email
        Mail::to($event->verify['key'])->send(new VerifyEmail($details));

//        dd(Mail::failures(), __METHOD__);
//        Mail::to($event->email)->send(new VerifyEmail($event->email));
//        return $this->view();
    }
}
