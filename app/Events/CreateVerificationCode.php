<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Auth\Classes\VerificationCode;

class CreateVerificationCode
{
    use SerializesModels;

    public $verificationCode;

    /**
     * Create a new event instance.
     *
     * @param string $model Customer
     * @param int $mobile 9351234567
     */
    public function __construct(string $model, int $mobile)
    {
        $this->verificationCode = VerificationCode::createCode($model, $mobile);
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
