<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Auth\Classes\VerificationCode;

class VerifyEmailEvent
{
    use SerializesModels;

    /**
     * @var mixed
     */
    public $verify;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(string $email)
    {
        $this->verify = VerificationCode::createCode($email);
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
