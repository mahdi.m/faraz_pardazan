<?php


namespace App\Facades;


use Illuminate\Database\Eloquent\Model;

/**
 * Class ResponderProviderFacade
 * @package App\Facades
 * @method create(Model $user, int $type, int $value, string $title, $transactionId = null)
 */
class TransactionProviderFacade extends BaseFacade
{
    const key = 'transactionProvider';
}
