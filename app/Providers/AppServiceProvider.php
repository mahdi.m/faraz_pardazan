<?php

namespace App\Providers;

use App\Facades\ResponderProviderFacade;
use App\Facades\TransactionProviderFacade;
use App\Services\Responses\ApiResponderProvider;
use App\Services\TransactionProvider;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        ResponderProviderFacade::shouldProxyTo(ApiResponderProvider::class);
        TransactionProviderFacade::shouldProxyTo(TransactionProvider::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Passport::routes();

        Passport::tokensCan([
            'customer' => 'login token for customer',
        ]);
    }
}
