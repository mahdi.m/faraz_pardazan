<?php

namespace App\Providers;

use App\Events\CreateVerificationCode;
use App\Events\VerifyEmailEvent;
use App\Events\VerifyMobileEvent;
use App\Listeners\SendVerificationCode;
use App\Listeners\VerifyEmailListener;
use App\Listeners\VerifyMobileListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        CreateVerificationCode::class => [
            SendVerificationCode::class,
        ],

        VerifyEmailEvent::class => [
            VerifyEmailListener::class,
        ],

        VerifyMobileEvent::class => [
            VerifyMobileListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
