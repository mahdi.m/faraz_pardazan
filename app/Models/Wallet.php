<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    use HasFactory;

    protected $fillable = [
        'customer_id',
        'value', // اعتبار کیف پول
        'blocked_value',
        'income_value', // کل مقداری که به حساب کاربر واریز شده
        'output_value', // کل مقداری که از حساب کاربر خارج شده
        'shopping_value', // مبلغ کل خریدهایی که کاربر انجام داده
        'shopping_return_value', // مبلغ کلی کخ از خرید ها به حسابش برگشته
        'payment_value', // کل مبلغی که تا الان واریز کرده
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
