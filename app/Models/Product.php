<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

//    protected $fillable = [
//        'category_id',
//        'title',
//        'desc',
//        'price',
//        'existence',
//        'is_new',
//        'special_discount',
//        'special',
//        'commentable', // امکان نظر دادن دارد یا نه
//        'discount', // میزان تخفیف
//        'discount_type', // نوع تخفیف. درصدی یا مبلغ ثابت
//    ];

    public function exchanges(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Exchange::class);
    }

    public function status(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Status::class);
    }
}
