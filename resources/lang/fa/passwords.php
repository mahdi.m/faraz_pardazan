<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'تغییر پسورد با موفقیت انجام شد',
    'sent' => 'لینک تغییر پسور ب ایمیل شماارسال شد',
    'token' => 'توکن برای تغییر پسورد معتبر نیست دوباره توکن بگیرین',
    'user' => "اردس ایمیل وجود ندارد",

];

