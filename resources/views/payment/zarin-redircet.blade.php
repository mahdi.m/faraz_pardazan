<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>تراکنش با موفقیت انجام شد</h1>
<h5>
    کد تراکنش:
    {{ $referenceId }}
</h5>
{{--<button onclick="alert('حالا باید اپلیکیشن باز بشه')">باز کردن اپلیکیشن</button>--}}

<a href="pet://pet.ir/payment/{{ $paymentId }}">باز کردن اپلیکیشن</a>
{{--<a href="seenshow://Home">باز کردن اپلیکیشن</a>--}}

</body>
</html>


{{--<!doctype html>--}}
{{--<html lang="en">--}}
{{--<head>--}}
{{--    <meta charset="UTF-8">--}}
{{--    <meta name="viewport"--}}
{{--          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">--}}
{{--    <meta http-equiv="X-UA-Compatible" content="ie=edge">--}}
{{--    <title>Document</title>--}}
{{--    <style>--}}
{{--        video {--}}
{{--            border: 1px solid red;--}}
{{--        }--}}
{{--    </style>--}}
{{--</head>--}}
{{--<body>--}}
{{--<h2>test video streaming</h2>--}}


{{--     روش اول       --}}
{{--<link href="https://cdnjs.cloudflare.com/ajax/libs/video.js/5.10.2/alt/video-js-cdn.css" rel="stylesheet">--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/video.js/5.10.2/video.js"></script>--}}
{{--<script src="https://cdn.jsdelivr.net/npm/hls.js@latest"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-hls/3.0.2/videojs-contrib-hls.js"></script>--}}
{{--<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>--}}

{{--<input type="text" id="url" style="width:100%" value="/test/any.m3u8">--}}
{{--<input type="text" id="url" style="width:100%" value="/test/any.m3u8">--}}
{{--<input type="button" id="j" value="play">--}}

{{--<br><br>--}}

{{--<video id="video" class="video-js vjs-default-skin" preload="none" crossorigin="true" controls width="640" height="268" controls>--}}
{{--</video>--}}

{{--<script>--}}
{{--    $(document).ready(function () {--}}
{{--        $("#j").on("click", function () {--}}
{{--            $("#video").html("<source src='" + $("#url").val() + "' type='application/x-mpegURL'>");--}}
{{--            var ply = videojs("video");--}}
{{--            ply.play();--}}
{{--        });--}}
{{--    });--}}
{{--</script>--}}


{{--    روش دوم     --}}
{{--<link href="https://cdnjs.cloudflare.com/ajax/libs/video.js/5.10.2/alt/video-js-cdn.css" rel="stylesheet">--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/video.js/5.10.2/video.js"></script>--}}


{{--<video id="video"></video>--}}
{{--<script src="https://cdn.jsdelivr.net/npm/hls.js@latest"></script>--}}
{{--<script>--}}

{{--    var video = document.getElementById('video');--}}
{{--    var videoSrc = "/test/any.m3u8";--}}
{{--    //--}}
{{--    // First check for native browser HLS support--}}
{{--    //--}}
{{--    if (video.canPlayType('application/vnd.apple.mpegurl')) {--}}
{{--        video.src = videoSrc;--}}
{{--        video.addEventListener('loadedmetadata', function() {--}}
{{--            video.play();--}}
{{--        });--}}
{{--        //--}}
{{--        // If no native HLS support, check if hls.js is supported--}}
{{--        //--}}
{{--    } else if (Hls.isSupported()) {--}}
{{--        var hls = new Hls();--}}
{{--        hls.loadSource(videoSrc);--}}
{{--        hls.attachMedia(video);--}}
{{--        hls.on(Hls.Events.MANIFEST_PARSED, function() {--}}
{{--            video.play();--}}
{{--        });--}}
{{--    }--}}
{{--</script>--}}
{{--</body>--}}
{{--</html>--}}
